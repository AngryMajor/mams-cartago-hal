package mams.artifacts;

import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.*;

public abstract class ListArtifact extends TypedResourceArtifact {
    protected ObjectMapper mapper = new ObjectMapper();
	
	@OPERATION
	void init(final String name, final String className) {
        this.name = name;
        this.className = className;
        // defineObsProperty("size", 0);
    }

    protected int size() {
        return this.handler.getLinks().size();
    }

    @OPERATION
    public void size(final OpFeedbackParam<Integer> size) {
        size.set(size());
    }
}
