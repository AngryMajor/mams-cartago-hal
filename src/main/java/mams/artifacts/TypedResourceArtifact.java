package mams.artifacts;

import java.util.HashMap;
import java.util.Map;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.command.CommandFactory;
import mams.web.WebServer;

public abstract class TypedResourceArtifact extends ResourceArtifact {
    protected Map<String, CommandFactory> factories = new HashMap<String,CommandFactory>();
    protected String className;

    @OPERATION
    void type(OpFeedbackParam<String> type) {
        type.set(className);
	}

	public String getType() {
		return className;
	}

    public boolean handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        String method = request.method().asciiName().toString();
        CommandFactory factory = factories.get(method);
        if (factory == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            try{
                if (!factory.create(this, ctx, request).execute()) {
                    WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.FORBIDDEN);
                }
            } catch(Throwable t){
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.BAD_REQUEST);
                t.printStackTrace();
            }
        }
        return true;
    }

    public String[] getAcceptedMethods() {
        String[] methods = new String[factories.size()];
        int i=0;
        for (String method : factories.keySet()) {
            methods[i++] = method;
        }
        return methods;
    }

	public ResourceArtifact[] getChildren() {
		return handler.getChildren();
	}
}
