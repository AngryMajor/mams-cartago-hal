package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;

public abstract class AbstractRequestCommand implements Command {
    protected static long CACHE_ID = 0;

    protected TypedResourceArtifact artifact;
    protected ChannelHandlerContext ctx;
    protected FullHttpRequest request;
    
    public AbstractRequestCommand(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request) {
        this.artifact = artifact;
        this.ctx = ctx;
        this.request = request;
    }

    protected String getMethod() {
        return request.method().asciiName().toString();
    }
}