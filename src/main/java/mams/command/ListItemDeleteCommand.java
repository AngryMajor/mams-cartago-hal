package mams.command;

import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.web.WebServer;

public class ListItemDeleteCommand extends AbstractItemCommand {
    static ObjectMapper mapper = new ObjectMapper();
    
    public ListItemDeleteCommand(ResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request, String type) {
        super(artifact, ctx, request, type);
    }

    public boolean execute() {
        try{
            CartagoBackend.getInstance().doAction(artifact.getId(), new Op("destroyArtifact"));
        } catch(Exception e){
            e.printStackTrace();
            WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "application/json", "");
            return false;
        }
        WebServer.writeResponse(ctx, request, HttpResponseStatus.NO_CONTENT, "application/json", "");
        return true;
    }
}