package mams.command;

import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.ArtifactId;
import cartago.OpFeedbackParam;
import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.TypedResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.WebServer;

public class PassiveListPostCommand implements Command {
    private static ObjectMapper mapper = new ObjectMapper();
    
    private TypedResourceArtifact artifact;
    private ChannelHandlerContext ctx;
    private FullHttpRequest request;
    private String type;

    public PassiveListPostCommand(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request,String type) {
        this.artifact = artifact;
        this.ctx = ctx;
        this.request = request;
        this.type = type;
	}

	@Override
    public boolean execute() {
        Object data = null;
        String name = null;
        
        try {
            data = mapper.readValue(Utils.getBody(request), Class.forName(type));
            name = Utils.getIdentifier(type,data);
        } catch (Exception e) {
            e.printStackTrace();
            WebServer.writeResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text", e.getMessage());
            return false;
        }
        
        try {
            OpFeedbackParam<ArtifactId> id = new  OpFeedbackParam<ArtifactId>();
            String qname = artifact.getId().getName()+"-"+name;
            CartagoBackend.getInstance().
                doAction(new Op("makeArtifact", qname, "mams.passive.PassiveListItemArtifact", new Object[] {name, type, data}, id));

            CartagoBackend.getInstance().doAction(new Op("linkArtifacts", id.get(), "out-1", artifact.getId()));
            CartagoBackend.getInstance().doAction(id.get(), new Op("createRoute"));
        } catch(Exception e){
            e.printStackTrace();
            WebServer.writeResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text", e.getMessage());
            return false;
        }
        
        WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/json", "");
        return true;
    }
    
}