package mams.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.artifacts.AbstractItemArtifact;
import mams.web.WebServer;

public class ItemGetCommand extends AbstractItemCommand {
    private static ObjectMapper mapper = new ObjectMapper();
    
    public ItemGetCommand(ResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request, String type) {
        super(artifact, ctx, request, type);
    }

    public boolean execute() {
        ObjectNode node = ((AbstractItemArtifact) artifact).createItemNode(new ResourceArtifact[]{});
        try {
            String json = mapper.writeValueAsString(node);
            WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/hal+json", json);
        } catch (JsonProcessingException e) {
            WebServer.writeResponse(ctx, request, HttpResponseStatus.BAD_REQUEST, "plain/text", e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }
}