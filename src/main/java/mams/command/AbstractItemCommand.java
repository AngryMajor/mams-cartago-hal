package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.ResourceArtifact;

public abstract class AbstractItemCommand implements Command {
    protected ResourceArtifact artifact;
    protected ChannelHandlerContext ctx;
    protected FullHttpRequest request;
    protected String type;
    
    public AbstractItemCommand(ResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request, String type) {
        this.artifact = artifact;
        this.ctx = ctx;
        this.request = request;
        this.type = type;
    }
}