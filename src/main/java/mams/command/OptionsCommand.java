package mams.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.TypedResourceArtifact;
import mams.web.WebServer;

public class OptionsCommand implements Command {
    private TypedResourceArtifact artifact;
    private ChannelHandlerContext ctx;
    private FullHttpRequest request;

    public OptionsCommand(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request) {
        this.artifact = artifact;
        this.ctx = ctx;
        this.request = request;
    }

    public boolean execute() {
        String[] methods = artifact.getAcceptedMethods();
        System.out.println("Methods: " + methods);
        // TODO: Need to construct appropriate response (existing methods may be insufficient)
        WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "plain/text","");
        return true;
    }
}