package mams.command;

public interface Command {
    boolean execute();
}