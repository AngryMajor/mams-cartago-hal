package mams.command;

import java.lang.reflect.Field;

import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.WebServer;

public class ItemPutCommand extends AbstractItemCommand {
    static ObjectMapper mapper = new ObjectMapper();
    
    public ItemPutCommand(ResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request, String type) {
        super(artifact, ctx, request, type);
    }

    public boolean execute() {
        try {
            Class<?> myClass = Class.forName(type);
            Object data = mapper.readValue(Utils.getBody(request), myClass);


            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                if (field.get(data) != null) 
                    CartagoBackend.getInstance().doAction(artifact.getId(), new Op("update", field.getName(),field.get(data)));
            }
            
            WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/json", "");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}