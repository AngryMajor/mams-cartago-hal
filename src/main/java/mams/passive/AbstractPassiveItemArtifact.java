package mams.passive;

import mams.artifacts.AbstractItemArtifact;
import mams.command.ItemGetFactory;

public class AbstractPassiveItemArtifact extends AbstractItemArtifact{
    {
        factories.put("GET", new ItemGetFactory());
    }
}