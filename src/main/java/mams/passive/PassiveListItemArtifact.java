package mams.passive;

import cartago.*;
import mams.command.ListItemDeleteFactory;
import mams.command.ListItemPutFactory;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListItemArtifact extends AbstractPassiveItemArtifact {
    {
        factories.put("DELETE", new ListItemDeleteFactory());
        factories.put("PUT", new ListItemPutFactory());
    }
}
