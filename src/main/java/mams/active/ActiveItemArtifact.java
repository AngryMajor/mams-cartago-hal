package mams.active;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import mams.command.ItemPutFactory;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveItemArtifact extends AbstractActiveItemArtifact {
    {
        requestFactories.put("PUT", new ItemPutFactory());
    }
}