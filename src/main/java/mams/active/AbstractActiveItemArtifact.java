package mams.active;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.active.command.ActiveItemRequestNoBodyCommandFactory;
import mams.artifacts.AbstractItemArtifact;
import mams.active.command.ActiveItemRequestCommandFactory;
import mams.command.CommandFactory;
import mams.command.ItemGetFactory;
import mams.active.HTTPRequest;
import mams.web.WebServer;

public class AbstractActiveItemArtifact extends AbstractItemArtifact {
    protected Map<String, CommandFactory> requestFactories = new HashMap<>();

    {
        requestFactories.put("GET", new ItemGetFactory());
        factories.put("PUT", new ActiveItemRequestCommandFactory());
        factories.put("GET", new ActiveItemRequestNoBodyCommandFactory());
    }

    private Map<Long, HTTPRequest> requestCache = new HashMap<>();
    
    @OPERATION
    void enqueue(HTTPRequest request) {
        requestCache.put(request.getId(), request);
        this.signal("newRequest", request.getId(),request.getMethod(), request.getType());
    }

    @OPERATION
    void updateEventField(long id, String fieldName, String value) {
        HTTPRequest event = requestCache.get(id);
        Object data = event.getData();
        try {
            Class<?> myClass = Class.forName(event.getType());

            Field field = myClass.getField(fieldName);
            field.set(data, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OPERATION
    void getEventFieldString(long id, String fieldName, OpFeedbackParam<Object> value) {
        HTTPRequest event = requestCache.get(id);
        Object data = event.getData();
        try {
            Class<?> myClass = Class.forName(event.getType());

            Field field = myClass.getField(fieldName);
            value.set(field.get(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OPERATION
    void acceptRequest(long id) {
        HTTPRequest event = requestCache.remove(id);
        CommandFactory factory = requestFactories.get(event.getMethod());
        if (factory == null) {
            WebServer.writeErrorResponse(event.getCtx(), event.getRequest(), HttpResponseStatus.FORBIDDEN);
        } else {
            factory.create(this, event).execute();
        }
    }
    @OPERATION
    void refuseRequest(long id) {
        HTTPRequest event = requestCache.remove(id);
        WebServer.writeErrorResponse(event.getCtx(), event.getRequest(), HttpResponseStatus.FORBIDDEN);
    }
}