package mams.active;

import mams.artifacts.ListArtifact;
import mams.command.PassiveListPostFactory;
import mams.web.WebServer;
import mams.active.HTTPRequest;
import mams.active.command.ActiveItemRequestCommandFactory;
import mams.active.command.ActiveItemRequestNoBodyCommandFactory;
import mams.command.CommandFactory;
import mams.command.PassiveListGetFactory;

import java.util.Map;
import java.util.HashMap;

import cartago.*;
import io.netty.handler.codec.http.HttpResponseStatus;


@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class ActiveListArtifact extends ListArtifact {
    protected Map<String, CommandFactory> requestFactories = new HashMap<>();

    {
        factories.put("POST", new ActiveItemRequestCommandFactory());
        factories.put("GET", new ActiveItemRequestNoBodyCommandFactory());
        requestFactories.put("POST", new PassiveListPostFactory());
        requestFactories.put("GET", new PassiveListGetFactory());
    }


    private Map<Long, HTTPRequest> requestCache = new HashMap<>();
    
    @OPERATION
    void enqueue(HTTPRequest request) {
        requestCache.put(request.getId(), request);
        this.signal("newRequest", request.getId(),request.getMethod(), request.getType());
    }

    @OPERATION
    void acceptRequest(long id) {
        HTTPRequest event = requestCache.remove(id);
        CommandFactory factory = requestFactories.get(event.getMethod());
        if (factory == null) {
            WebServer.writeErrorResponse(event.getCtx(), event.getRequest(), HttpResponseStatus.FORBIDDEN);
        } else {
            factory.create(this, event).execute();
        }
    }
    
    @OPERATION
    void refuseRequest(long id) {
        HTTPRequest event = requestCache.remove(id);
        WebServer.writeErrorResponse(event.getCtx(), event.getRequest(), HttpResponseStatus.FORBIDDEN);
    }
}


