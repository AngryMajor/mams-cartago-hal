package mams.active.command;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import mams.artifacts.TypedResourceArtifact;
import mams.command.Command;
import mams.command.CommandFactory;
import mams.active.HTTPRequest;

public class ActiveItemRequestCommandFactory implements CommandFactory {

    @Override
    public Command create(TypedResourceArtifact artifact, HTTPRequest event) {
        return null;
    }

    @Override
    public Command create(TypedResourceArtifact artifact, ChannelHandlerContext ctx, FullHttpRequest request) {
        return new ActiveItemRequestCommand(artifact, ctx, request);
    }

}